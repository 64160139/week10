package com.fah.week10;

public class Triangle extends Shape {
    private double a;
    private double b;
    private double c;
    public double s = 0 ;

    public Triangle(double a, double b, double c) {
        super("Triangle");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    public double getA(){
        return a;
    }
    public double getB(){
        return b;
    }
    public double getC(){
        return c;
    }

    @Override
    public double calArea() {
        s = (a + c + b)/2;
        return  Math.sqrt(s*(s-a)*(s-b)*(s-c));
    }

    @Override
    public double calPerimeter() {
        return (a+b+c) ;
    }
    @Override 
    public String toString(){
        return this.getName() + " A = "+ this.a + " b = "+this.b +" C = "+this.c;
    }
}
