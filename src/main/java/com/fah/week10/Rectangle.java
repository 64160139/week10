package com.fah.week10;

public class Rectangle extends Shape{
    private double widgth ;
    private double height ;

    public Rectangle(double widgth , double height){
        super("Retangle");
        this.widgth = widgth;
        this.height = height;
    }
    public double getWidgth(){
        return widgth;
    }
    public double getHeight(){
        return height;
    }
    @Override 
    public String toString(){
        return this.getName() + " widgth = "+ this.widgth + " Height = "+this.height;
    }

    @Override
    public double calArea(){
        return this.widgth * this.height ;
    }
    @Override
    public double calPerimeter(){
        return (this.widgth + this.height) * 2 ;
    }
}
