package com.fah.week10;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimeter());
        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimeter());
        System.out.println("-----------------------------------");
        // -----------------------------------//
        Circle cir1 = new Circle(5);
        System.out.println(cir1);
        System.out.printf("%s area = %.3f \n", cir1.getName() ,cir1.calArea());
        System.out.printf("%s perimeter = %.3f \n", cir1.getName() ,cir1.calPerimeter());
        Circle cir2 = new Circle(12);
        System.out.println(cir2);
        System.out.printf("%s area = %.3f \n",cir2.getName(),cir2.calArea());
        System.out.printf("%s perimeter = %.3f \n",cir2.getName(),cir2.calPerimeter());
        System.out.println("-----------------------------------");
        // -----------------------------------//
        Triangle triang1 = new Triangle(5, 5, 6);
        System.out.println(triang1);
        System.out.printf("%s area = %.2f \n",triang1.getName(),triang1.calArea());
        System.out.printf("%s perimeter = %.2f \n",triang1.getName(),triang1.calPerimeter());
        Triangle triang2 = new Triangle(3, 4, 5);
        System.out.println(triang1);
        System.out.printf("%s area = %.2f \n",triang2.getName(),triang2.calArea());
        System.out.printf("%s perimeter = %.2f \n",triang2.getName(),triang2.calPerimeter());

    }
}
